#include <stdio.h>
 void pares(){
    int a;
    printf("Introduza o numero maximo\n");
    scanf("%d", &a);

    for (int i=a; i>=2; i--){
        if(i % 2 == 0){
            printf("%d \n", i);
        }
    }
}

int par()
{
    int num;
    printf("Escolha um numero: ");
    scanf("%d", &num);

    if(num % 2 == 0) {
        printf("%d e par.", num);
        return 0;
    }
    else {
        printf("%d e impar.", num);
        return 1;
    }
}

int divisivel(){
    int a, b;
    printf("Numero a dividir: ");
    scanf("%d", &a);
    printf("Numero divisor: ");
    scanf("%d", &b);

    if(a % b == 0){
        printf("%d e divisivel");
        return 0;
    }
    else{
        printf("%d nao e divisivel");
    }
}

int divisores(){
    int num;
    printf("Introduza o numero\n");
    scanf("%d", &num);
    for(int i=1; i<=num; i++)
    {
        if(num%i==0){
            printf("%d\n", i);
        }
    }
}

int primos(){
    int num;
    int cd=0;
    printf("Introduza o numero\n");
    scanf("%d", &num);
    for(int i=1; i<=num; i++){
        if(num%i==0)
            cd++;
    }
    if(cd==2){
        printf("e primo");
    }
    else
        printf("nao e primo");
}

int main(){
    int p;
    printf("1-Funcao numeros pares ate 2\n2-Numero par ou impar\n3-Numero 'a' e divisivel por numero 'b'\n4-Divisores de um numero\n5-Numero primo ou nao\n ");
    scanf("%d", &p);

    switch(p) {
        case 1:
            divisivel();
        break;

        case 2:
            par();
        break;

        case 3:
            pares();
        break;

        case 4:
            divisores();
        break;

        case 5:
            primos();
        break;
    }
}